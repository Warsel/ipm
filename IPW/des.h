#pragma once
#include <string>
#include <bitset>
#include <stdlib.h>
#include <tuple>

using std::string;
using std::tuple;
using std::bitset;

tuple<string, string> encode_des(string message, string key);
tuple<string, string> decode_des(string message, string key);
void perform_message_and_key_des(string* message, string* key);
tuple<string, string> get_message_and_key_in_binary_format(string message, string key);
string string_to_binary_format(string str);
string binary_format_to_string(string str);
string encode_des_round(string binary_message_block, string binary_key);
string decode_des_round(string binary_message_block, string binary_key);
string XOR(string str_1, string str_2);
string f(string str_1, string str_2);
string shift_key_right(string binary_key);
string shift_key_left(string binary_key);
