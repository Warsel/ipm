#pragma once

#include <iostream>
#include <fstream>
#include <string>

using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::getline;

void des();
void gost_28147();
void rsa();
string read_file(string file_name);
void write_file(string data, string file_name);