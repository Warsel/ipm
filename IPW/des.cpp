#include "DES.h"

constexpr auto BLOCK_SIZE = 64;
constexpr auto CHAR_SIZE = 8;
constexpr auto KEY_SHIFT = 2;
constexpr auto ROUNDS_COUNT = 16;
constexpr auto KEY_SIZE_DES = 56;


tuple<string, string> encode_des(string message, string key) {
	perform_message_and_key_des(&message, &key);

	string binary_message;
	string binary_key;
	std::tie(binary_message, binary_key) = get_message_and_key_in_binary_format(message, key);

	size_t blocks_count = binary_message.length() / BLOCK_SIZE;
	string* blocks = new string[blocks_count];

	for (size_t i = 0; i < blocks_count; i++) {
		blocks[i] = binary_message.substr(i * BLOCK_SIZE, BLOCK_SIZE);
	}

	for (size_t i = 0; i < ROUNDS_COUNT; i++) {
		for (size_t i = 0; i < blocks_count; i++) {
			blocks[i] = encode_des_round(blocks[i], binary_key);
		}

		if (i != ROUNDS_COUNT - 1) {
			binary_key = shift_key_right(binary_key);
		}
	}

	string encoded_message;
	string encoded_key;

	for (size_t i = 0; i < blocks_count; i++) {
		encoded_message += binary_format_to_string(blocks[i]);
	}

	encoded_key = binary_format_to_string(binary_key);

	delete[] blocks;

	return std::make_tuple(encoded_message, encoded_key);
}

tuple<string, string> decode_des(string message, string key) {
	string binary_message;
	string binary_key;
	std::tie(binary_message, binary_key) = get_message_and_key_in_binary_format(message, key);

	size_t blocks_count = binary_message.length() / BLOCK_SIZE;
	string* blocks = new string[blocks_count];

	for (size_t i = 0; i < blocks_count; i++) {
		blocks[i] = binary_message.substr(i * BLOCK_SIZE, BLOCK_SIZE);
	}

	for (size_t i = 0; i < ROUNDS_COUNT; i++) {
		for (size_t i = 0; i < blocks_count; i++) {
			blocks[i] = decode_des_round(blocks[i], binary_key);
		}

		if (i != ROUNDS_COUNT - 1) {
			binary_key = shift_key_left(binary_key);
		}
	}

	string decoded_message;
	string decoded_key;

	for (size_t i = 0; i < blocks_count; i++) {
		decoded_message += binary_format_to_string(blocks[i]);
	}

	decoded_key = binary_format_to_string(binary_key);

	delete[] blocks;

	return std::make_tuple(decoded_message, decoded_key);
}

string encode_des_round(string binary_message_block, string binary_key) {
	size_t binary_message_block_half_length = binary_message_block.length() / 2;
	string l = binary_message_block.substr(0, binary_message_block_half_length);
	string r = binary_message_block.substr(binary_message_block_half_length, binary_message_block_half_length);

	return r + XOR(l, f(r, binary_key));
}

string decode_des_round(string binary_message_block, string binary_key) {
	size_t binary_message_block_half_length = binary_message_block.length() / 2;
	string l = binary_message_block.substr(0, binary_message_block_half_length);
	string r = binary_message_block.substr(binary_message_block_half_length, binary_message_block_half_length);

	return XOR(f(l, binary_key), r) + l;
}

string shift_key_right(string binary_key) {
	for (size_t i = 0; i < KEY_SHIFT; i++) {
		binary_key.insert(0, &binary_key.back());
		binary_key.pop_back();
	}

	return binary_key;
}

string shift_key_left(string binary_key) {
	for (size_t i = 0; i < KEY_SHIFT; i++) {
		binary_key.push_back(binary_key[0]);
		binary_key.erase(0, 1);
	}

	return binary_key;
}

string XOR(string str_1, string str_2) {
	string result = "";
	size_t str_length = str_1.length();

	for (size_t i = 0; i < str_length; i++) {
		result += str_1[i] ^ str_2[i] ? "1" : "0";
	}

	return result;
}

string f(string str_1, string str_2) {
	return XOR(str_1, str_2);
}

tuple<string, string> get_message_and_key_in_binary_format(string message, string key) {
	string binary_message = string_to_binary_format(message);
	string binary_key = string_to_binary_format(key);
	
	return std::make_tuple(binary_message, binary_key);
}

string string_to_binary_format(string str) {
	string result = "";
	size_t str_length = str.length();

	for (size_t i = 0; i < str_length; i++) {
		result += bitset<CHAR_SIZE>(str[i]).to_string();
	}

	return result;
}

string binary_format_to_string(string str) {
	string result = "";
	size_t block_length = str.length() / CHAR_SIZE;
	string symbol;

	for (size_t i = 0; i < block_length; i++) {
		symbol = str.substr(i * CHAR_SIZE, CHAR_SIZE);
		result += static_cast<char>(strtoull(symbol.c_str(), 0, 2));
	}

	return result;
}

void perform_message_and_key_des(string* message, string* key) {
	while ((message->length() * CHAR_SIZE) % BLOCK_SIZE) {
		message->append("_");
	}

	if (key->length() * CHAR_SIZE > KEY_SIZE_DES) {
		key->resize(KEY_SIZE_DES / CHAR_SIZE);
	}
	else {
		while (key->length() * CHAR_SIZE < KEY_SIZE_DES) {
			key->insert(0, "!");
		}
	}
}