#include "rsa.h"

string encode_rsa(string message, pair<uint64_t, uint64_t> public_key) {
	size_t message_length = message.length();
	std::stringstream string_stream;
	
	uint64_t e = public_key.first;
	uint64_t n = public_key.second;

	for (size_t i = 0; i < message_length; i++) {
		string_stream << binary_pow(message[i], e, n) << " ";
		string_stream.clear();
	}

	return string_stream.str();
}

string decode_rsa(string message, pair<uint64_t, uint64_t> private_key) {
	std::stringstream string_stream_encoded_message(message);
	string decoded_message = "";

	uint64_t d = private_key.first;
	uint64_t n = private_key.second;

	uint64_t current_value;

	while (string_stream_encoded_message >> current_value) {
		decoded_message += binary_pow(current_value, d, n);
	}

	return decoded_message;
}

pair<pair<uint64_t, uint64_t>, pair<uint64_t, uint64_t>> generate_keys(uint32_t p, uint32_t q) {
	uint64_t n = (uint64_t)p * (uint64_t)q;
	uint64_t f_n = (uint64_t)(p - 1) * (uint64_t)(q - 1);
	uint64_t d = generate_d(f_n);
	uint64_t e = generate_e(f_n, d);

	return { {e, n}, {d, n} };
}

uint64_t generate_d(uint64_t f_n) {
	uint64_t gcd;

	for (uint64_t d = f_n - 2; d > 0; d--) {
		gcd = euclidean_algorithm_gcd(f_n, d);

		if (gcd == 1) {
			return d;
		}
	}

	return -1;
}

uint64_t generate_e(uint64_t f_n ,uint64_t d) {
	uint64_t e = 1;

	while ((e * d) % f_n != 1) {
		e++;
	}

	return e;
}

uint64_t euclidean_algorithm_gcd(uint64_t a, uint64_t b) {
	return b ? euclidean_algorithm_gcd(b, a % b) : a;
}

vector<uint16_t> get_prime_numbers() {  // sieve_of_eratosthenes
	uint64_t n = UINT8_MAX;

	uint64_t* primes = new uint64_t[n + 1];

	for (uint64_t i = 0; i < n; i++) {
		primes[i] = true;
	}

	primes[0] = false;
	primes[1] = false;

	for (uint64_t i = 2; i <= n; i++) {
		if (primes[i]) {
			if (i * i <= n) {
				for (uint64_t j = i * i; j <= n; j += i) {
					primes[j] = false;
				}
			}
		}
	}

	vector<uint16_t> prime_numbers;

	for (uint64_t i = 0; i <= n; i++) {
		if (primes[i]) {
			prime_numbers.push_back(i);
		}
	}

	return prime_numbers;
}

pair<uint16_t, uint16_t> generate_p_and_q() {
	srand(time(nullptr));
	vector<uint16_t> prime_numbers = get_prime_numbers();
	uint16_t random_index_1 = rand() % prime_numbers.size();
	uint16_t random_index_2 = rand() % prime_numbers.size();

	uint16_t p = prime_numbers[random_index_1];
	uint16_t q = prime_numbers[random_index_2];

	return { p, q };
}

uint64_t binary_pow(uint64_t x, uint64_t e, uint64_t n) {
	if (e == 0) {
		return 1;
	}
	if (e % 2 == 1) {
		return (binary_pow(x, e - 1, n) * x) % n;
	}
	else {
		uint64_t t = (binary_pow(x, e / 2, n)) % n;
		return t * t;
	}
}