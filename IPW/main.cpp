#include "main.h"
#include "des.h"
#include "gost_28147.h"
#include "rsa.h"

#define DEBUG


int main() {
	cout << "DES:" << endl;
	des();
	cout << "GOST_28147:" << endl;
	gost_28147();
	cout << "RSA:" << endl;
	rsa();
}

void des() {
#ifdef DEBUG
	string message = "Some secret message. Some secret message. Some secret message. Some secret message. Some secret message. Some secret message.";
	cout << "Message:" << endl;
	cout << message << endl;
#else
	string message = read_file("input.txt");
#endif
	string key;

	cout << "Enter secret key:" << endl;
	getline(cin, key);

	string encoded_message;
	string encoded_key;
	std::tie(encoded_message, encoded_key) = encode_des(message, key);

#ifdef DEBUG
	string decoded_message;
	string decoded_key;

	cout << "\nEncoded message:" << endl;
	cout << encoded_message << endl;
	cout << "Encoded key:" << endl;
	cout << encoded_key << endl;

	std::tie(decoded_message, decoded_key) = decode_des(encoded_message, encoded_key);

	cout << "\nDecoded message:" << endl;
	cout << decoded_message << endl;
	cout << "Decoded key:" << endl;
	cout << decoded_key << endl;
#else
	write_file(encoded_message, "output_encoded.txt");
	write_file(encoded_key, "encoded_key.txt");

	encoded_message = read_file("output_encoded.txt");
	encoded_key = read_file("encoded_key.txt");

	string decoded_message;
	string decoded_key;
	std::tie(decoded_message, decoded_key) = decode_des(encoded_message, encoded_key);

	write_file(decoded_message, "output_decoded.txt");
#endif

	cout << "\nSuccessful. Press Enter to continue" << endl;
	getchar();
}

void gost_28147() {
#ifdef DEBUG
	string message = "Some secret message. Some secret message. Some secret message. Some secret message. Some secret message. Some secret message.";
	cout << "Message:" << endl;
	cout << message << endl;
#else
	string message = read_file("input.txt");
#endif
	string key;

	cout << "Enter secret key:" << endl;
	getline(cin, key);

	string encoded_message = encode_gost_28147(message, key);

#ifdef DEBUG
	cout << "\nEncoded message:" << endl;
	cout << encoded_message << endl;

	string decoded_message = decode_gost_28147(encoded_message, key);

	cout << "\nDecoded message:" << endl;
	cout << decoded_message << endl;
#else
	write_file(encoded_message, "output_encoded.txt");

	encoded_message = read_file("output_encoded.txt");

	string decoded_message = decode_gost_28147(encoded_message, key);
	write_file(decoded_message, "output_decoded.txt");
#endif

	cout << "\nSuccessful. Press Enter to continue" << endl;
	getchar();
}

void rsa() {
#ifdef DEBUG
	string message = "Some secret message. Some secret message. Some secret message. Some secret message. Some secret message. Some secret message.";
	cout << "Message:" << endl;
	cout << message << endl;
#else
	string message = read_file("input.txt");
#endif

	auto prime_numbers = generate_p_and_q();

	auto keys = generate_keys(prime_numbers.first, prime_numbers.second);
	auto public_key = keys.first;
	auto private_key = keys.second;

	cout << "Public key: (" << public_key.first << " " << public_key.second << ")" << endl;
	cout << "Private key: (" << private_key.first << " " << private_key.second << ")" << endl;

	string encoded_message = encode_rsa(message, public_key);

#ifdef DEBUG
	cout << "\nEncoded message:" << endl;
	cout << encoded_message << endl;

	string decoded_message = decode_rsa(encoded_message, private_key);

	cout << "\nDecoded message:" << endl;
	cout << decoded_message << endl;
#else
	write_file(encoded_message, "output_encoded.txt");

	encoded_message = read_file("output_encoded.txt");

	string decoded_message = decode_rsa(encoded_message, private_key);
	write_file(decoded_message, "output_decoded.txt");
#endif
	cout << "\nSuccessful. Press Enter to continue" << endl;
	getchar();
}

string read_file(string file_name) {
	string line;
	string data = "";

	std::ifstream in;
	in.open(file_name);

	if (in.is_open())
	{
		while (getline(in, line))
		{
			data += line;
		}
	}

	in.close();

	return data;
}

void write_file(string data, string file_name) {
	std::ofstream out;
	out.open(file_name);

	if (out.is_open())
	{
		out << data;
	}

	out.close();
}