#include "gost_28147.h"

constexpr auto BLOCK_SIZE = 64;
constexpr auto CHAR_SIZE = 8;
constexpr auto KEY_SIZE = 256;
constexpr auto SUB_KEYS_SIZE = 8;
constexpr auto ROUNDS_COUNT = 32;


string encode_gost_28147(string message, string key) {
	perform_message_and_key(&message, &key);

	uint32_t* sub_keys = generate_sub_keys(key);

	size_t blocks_count = message.length() * CHAR_SIZE / BLOCK_SIZE;
	string* blocks = new string[blocks_count];
	for (size_t i = 0; i < blocks_count; i++) {
		blocks[i] = encode_gost_28147_round(message.substr(i * CHAR_SIZE, CHAR_SIZE), sub_keys);
	}

	string result = "";
	for (size_t i = 0; i < blocks_count; i++) {
		result += binary_format_to_string_gost(blocks[i]);
	}

	return result;
}

string decode_gost_28147(string message, string key) {
	perform_message_and_key(&message, &key);

	uint32_t* sub_keys = generate_sub_keys(key);

	size_t blocks_count = message.length() * CHAR_SIZE / BLOCK_SIZE;
	string* blocks = new string[blocks_count];
	for (size_t i = 0; i < blocks_count; i++) {
		blocks[i] = decode_gost_28147_round(message.substr(i * CHAR_SIZE, CHAR_SIZE), sub_keys);
	}

	string result = "";
	for (size_t i = 0; i < blocks_count; i++) {
		result += binary_format_to_string_gost(blocks[i]);
	}

	return result;
}

uint32_t* generate_sub_keys(string key) {
	uint32_t* sub_keys = new uint32_t[SUB_KEYS_SIZE];
	string binary_key = string_to_binary_format_gost(key);
	size_t size_block_half = BLOCK_SIZE / 2;

	for (size_t i = 0; i < SUB_KEYS_SIZE; i++) {
		sub_keys[i] = strtoul(binary_key.substr(i * size_block_half, size_block_half).c_str(), 0, 2);
	}

	return sub_keys;
}

string encode_gost_28147_round(string block_message, uint32_t* sub_keys) {
	string block_message_binary = string_to_binary_format_gost(block_message);
	size_t block_message_binary_length_half = block_message_binary.length() / 2;

	uint32_t a = strtoul(block_message_binary.substr(0, block_message_binary_length_half).c_str(), 0, 2);
	uint32_t b = strtoul(block_message_binary.substr(block_message_binary_length_half, block_message_binary_length_half).c_str(), 0, 2);

	size_t index;
	uint32_t temp;

	for (size_t i = 0; i < ROUNDS_COUNT; i++) {
		index = i < 24 ? i % SUB_KEYS_SIZE : 7 - i % SUB_KEYS_SIZE;
		temp = b ^ f_gost_28147(a, index, sub_keys);

		if (i < ROUNDS_COUNT - 1) {
			b = a;
			a = temp;
		}
		else {
			b = temp;
		}
	}

	string result = bitset<32>(a).to_string() + bitset<32>(b).to_string();
	return result;
}

string decode_gost_28147_round(string block_message, uint32_t* sub_keys) {
	string block_message_binary = string_to_binary_format_gost(block_message);
	size_t block_message_binary_length_half = block_message_binary.length() / 2;

	uint32_t a = strtoul(block_message_binary.substr(0, block_message_binary_length_half).c_str(), 0, 2);
	uint32_t b = strtoul(block_message_binary.substr(block_message_binary_length_half, block_message_binary_length_half).c_str(), 0, 2);

	size_t index;
	uint32_t temp;

	for (size_t i = 0; i < ROUNDS_COUNT; i++) {
		index = i < 8 ? i % SUB_KEYS_SIZE : 7 - i % SUB_KEYS_SIZE;
		temp = b ^ f_gost_28147(a, index, sub_keys);

		if (i < ROUNDS_COUNT - 1) {
			b = a;
			a = temp;
		}
		else {
			b = temp;
		}
	}

	string result = bitset<32>(a).to_string() + bitset<32>(b).to_string();
	return result;
}

uint32_t substitute(uint32_t value) {
	size_t index;
	uint32_t result = 0;

	for (size_t i = 0; i < SUB_KEYS_SIZE; i++) {
		index = (char)(value >> (4 * i) & 0x0f);
		result |= (uint32_t)(S_BOX[i][index] << (4 * i));
	}

	return result;
}

uint32_t f_gost_28147(uint32_t value, size_t index, uint32_t* sub_keys) {
	value = (value + sub_keys[index]) % ULONG_MAX;
	value = substitute(value);
	value = (value << 11) | (value >> 21);

	return value;
}

void perform_message_and_key(string* message, string* key) {
	while ((message->length() * CHAR_SIZE) % BLOCK_SIZE) {
		message->append("_");
	}

	if (key->length() * CHAR_SIZE > KEY_SIZE) {
		key->resize(KEY_SIZE);
	}
	else {
		while (key->length() * CHAR_SIZE < KEY_SIZE) {
			key->insert(0, "!");
		}
	}
}

string string_to_binary_format_gost(string str) {
	string result = "";
	size_t str_length = str.length();

	for (size_t i = 0; i < str_length; i++) {
		result += bitset<CHAR_SIZE>(str[i]).to_string();
	}

	return result;
}

string binary_format_to_string_gost(string str) {
	string result = "";
	size_t block_length = str.length() / CHAR_SIZE;
	string symbol;

	for (size_t i = 0; i < block_length; i++) {
		symbol = str.substr(i * CHAR_SIZE, CHAR_SIZE);
		result += static_cast<char>(strtoull(symbol.c_str(), 0, 2));
	}

	return result;
}