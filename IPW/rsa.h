#pragma once
#include <tuple>
#include <string>
#include <ctime>
#include <cmath>
#include <vector>
#include <sstream>

using std::string;
using std::vector;
using std::pair;

string encode_rsa(string message, pair<uint64_t, uint64_t> public_key);
string decode_rsa(string message, pair<uint64_t, uint64_t> private_key);
pair<pair<uint64_t, uint64_t>, pair<uint64_t, uint64_t>> generate_keys(uint32_t p, uint32_t q);
uint64_t generate_d(uint64_t f_n);
uint64_t generate_e(uint64_t f_n, uint64_t d);
uint64_t euclidean_algorithm_gcd(uint64_t a, uint64_t b);
vector<uint16_t> get_prime_numbers();
pair<uint16_t, uint16_t> generate_p_and_q();
uint64_t binary_pow(uint64_t x, uint64_t e, uint64_t n);