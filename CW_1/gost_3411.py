from const import *

GEN_64 = range(64)


def main():
    message = input('Enter message: ')
    hash_256 = get_hash(message, mode=256)
    hash_512 = get_hash(message)

    print(f'h256: {hash_256}')
    print(f'h512: {hash_512}')


def get_hash(message, mode=512):
    h = [0 if mode == 512 else 1 for _ in GEN_64]
    n = [0 for _ in GEN_64]
    _e = [0 for _ in GEN_64]

    while True:
        message_length = len(message)

        if message_length >= 64:
            m = get_byte_from_string(message[-64:])
            h = g(n, m, h)
            n = add_module_512(n, [64 for _ in GEN_64])
            _e = add_module_512(_e, m)
            message = message[:message_length - 64]
            continue

        break

    message_length = len(message)
    m = get_byte_from_string(message[-64:])
    fill_message(m)

    h = g(n, m, h)
    n = add_module_512(n, [message_length * 8 for _ in GEN_64])
    _e = add_module_512(_e, m)
    h = g([0 for _ in GEN_64], h, n)
    h = g([0 for _ in GEN_64], h, _e)

    if mode == 512:
        return ''.join(reversed(['%0.2x' % value for value in h][:64]))
    if mode == 256:
        return ''.join(reversed(['%0.2x' % value for value in h][:32]))

    raise Exception('Wrong mode')


def g(n, m, h):
    k = x_conversion(n, h)
    k = s_conversion(k)
    k = p_conversion(k)
    k = l_conversion(k)

    t = e(k, m)
    _g = x_conversion(t, m)

    return _g


def e(k, m):
    state = x_conversion(k, m)

    for i in range(12):
        state = s_conversion(state)
        state = p_conversion(state)
        state = l_conversion(state)

        k = get_next_key(k, i)
        state = x_conversion(state, k)

    return state


def x_conversion(value_1, value_2):
    return [value_1[i] ^ value_2[i] for i in range(len(value_1))]


def s_conversion(state):
    return [s_pi[state[i]] for i in GEN_64]


def p_conversion(state):
    return [state[p_tau[i]] for i in GEN_64]


def l_conversion(state):
    result = [0 for _ in GEN_64]

    for i in reversed(range(8)):
        for n in range(8):
            index = 63

            for j in reversed(range(8)):
                for k in range(8):
                    if (state[i * 8 + j] >> k) & 1:
                        result[i * 8 + n] ^= l_a[index * 8 + n]
                    index -= 1

    return result


def get_next_key(k, i):
    k = x_conversion(k, c[i])
    k = s_conversion(k)
    k = p_conversion(k)
    k = l_conversion(k)

    return k


def fill_message(m):
    if len(m) == 64:
        return

    m.insert(0, 0x1)

    while len(m) != 64:
        m.insert(0, 0x0)


def get_byte_from_string(message_block):
    m = []

    for i in reversed(range(len(message_block))):
        m.append(ord(message_block[i]))

    return m


def add_module_512(value_1, value_2):
    result = [None for _ in GEN_64]
    t = 0

    for i in reversed(GEN_64):
        t = value_1[i] + value_2[i] + (t >> 8)
        result[i] = (t & 0xFF)

    return result


if __name__ == '__main__':
    main()
